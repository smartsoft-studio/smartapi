﻿using SmartApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Areas.Business.Data
{
    public class MyAppsSearchDto: CoreBaseSearch
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
