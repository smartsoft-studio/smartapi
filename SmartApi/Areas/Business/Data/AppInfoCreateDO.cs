﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Areas.Business.Data
{
    public class AppInfoCreateDO
    {
        /// <summary>
        /// 应用业务类型
        /// </summary>
        public string AppBusinessType { get; set; }

        /// <summary>
        /// 应用业务编号
        /// </summary>
        public string AppBusinessNo { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public string AppVersion { get; set; }
    }
}
