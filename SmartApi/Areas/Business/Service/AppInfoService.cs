﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SmartApi.Areas.Business.Data;
using SmartApi.Areas.Business.Models;
using SmartApi.Models;
using SmartApi.Repository;
using SmartApi.Util.zip;
using SqlSugar;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace SmartApi.Areas.Business.Services
{
    public class AppInfoService: CoreBaseRepository<AppInfo>
    {
        #region initialize
        private readonly IConfiguration _configuration;
        private readonly ZipUtil _zipUtil;
        public AppInfoService(IConfiguration configuration)
        {
            _configuration = configuration;
            _zipUtil = new ZipUtil();
        }
        #endregion

        #region GetAll
        /// <summary>
        /// GetAll By Page
        /// </summary>
        /// <param name="searchDto"></param>
        /// <returns></returns>
        //public CoreVO<List<AppInfo>> GetAll(AppInfoSearchDto searchDto)
        //{
        //    try
        //    {
        //        int total = 0;
        //        var list = Db.Queryable<AppInfo>()
        //            .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.AppInfo}")
        //            .WhereIF(!string.IsNullOrWhiteSpace(searchDto.Name), x => x.Name.Contains(searchDto.Name))
        //            .ToPageList(searchDto.PageIndex, searchDto.PageSize, ref total);

        //        return new CoreVO<List<AppInfo>>() {
        //            Code = list.Any() ? "0X00" : "0X01",
        //            Msg = list.Any() ? $"{total}" : "没有找到数据",
        //            Success = true,
        //            Data = list.Any() ? list : null
        //        };
        //    }
        //    catch (Exception e)
        //    {
        //        return new CoreVO<List<AppInfo>>()
        //        {
        //            Code = "0X01",
        //            Success = false,
        //            Msg = e.Message,
        //            Data = null
        //        };
        //    }

        //}
        #endregion

        #region CheckUpdate
        /// <summary>
        /// CheckUpdate
        /// </summary>
        /// <param name="MD5"></param>
        /// <returns></returns>
        public CoreVO<AppInfo> CheckUpdate(string MD5)
        {
            try
            {
                var rs = Db.Queryable<AppInfo>()
                .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.APPINFO}")
                .OrderBy(x=>x.CreateTime,OrderByType.Desc).First();

                return new CoreVO<AppInfo>()
                {
                    Code = rs != null ? "0X00" : "0X01",
                    Success = rs != null,
                    Msg = rs != null && rs.AppMD5 != MD5 ? "检测到新版本" : "已是最新版",
                    Data = rs != null && rs.AppMD5 != MD5 ? rs : null
                };

            }
            catch (Exception e)
            {
                return new CoreVO<AppInfo>()
                {
                    Code = "0X01",
                    Success = false,
                    Msg = e.Message,
                    Data = null
                };
            }

        }
        #endregion

        #region Upload
        /// <summary>
        /// Upload
        /// </summary>
        /// <param name="file"></param>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public CoreVO<string> Upload(IFormFile file,AppInfoCreateDO model, CoreLoginUser user)
        {
            try
            {
                CoreVO<string> coreVO = new CoreVO<string>();
                string md5value = null;
                string version = null;
                if (file == null)
                {
                    coreVO.Success = false;
                    coreVO.Msg = "上传失败，请选择要上传的文件！";
                    return coreVO;
                }
                else
                {
                    string path = $"{CoreSettings._uploadPath}{Path.DirectorySeparatorChar}{model.AppBusinessType}{Path.DirectorySeparatorChar}{DateTime.Now.Year.ToString()}{Path.DirectorySeparatorChar}{DateTime.Now.Month.ToString()}{Path.DirectorySeparatorChar}{model.AppBusinessNo}";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    string fullpath = Path.Combine(path, Path.GetFileName(file.FileName));
                    using FileStream fileStream = new FileStream(fullpath, FileMode.Create);
                    file.CopyTo(fileStream);
                    fileStream.Close();

                    var result = _zipUtil.UnZip(fullpath, $"./wwwroot/temp/{Path.GetFileNameWithoutExtension(file.FileName)}/");
                    if (result)
                    {
                        string tempmd5 = null;
                        FileStream appmd5filestream = new FileStream($"./wwwroot/temp/{Path.GetFileNameWithoutExtension(file.FileName)}/SmartTools.Net.exe", FileMode.Open, FileAccess.Read, FileShare.Read);
                        FileStream dllmd5filestream = new FileStream($"./wwwroot/temp/{Path.GetFileNameWithoutExtension(file.FileName)}/SmartTools.Net.dll", FileMode.Open, FileAccess.Read, FileShare.Read);
                        //System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                        MD5 mD5 = MD5.Create();
                        tempmd5 = Convert.ToBase64String(mD5.ComputeHash(appmd5filestream));
                        tempmd5 += Convert.ToBase64String(mD5.ComputeHash(dllmd5filestream));
                        appmd5filestream.Close();
                        dllmd5filestream.Close();
                        md5value= Convert.ToBase64String(mD5.ComputeHash(Encoding.UTF8.GetBytes(tempmd5)));

                        //FileVersionInfo fileversioninfo = FileVersionInfo.GetVersionInfo($"./wwwroot/temp/{Path.GetFileNameWithoutExtension(file.FileName)}/SmartTools.Net.exe");

                        //Assembly assembly = Assembly.LoadFrom($"./wwwroot/temp/{Path.GetFileNameWithoutExtension(file.FileName)}/SmartTools.Net.dll");
                        //Version ver = assembly.GetName().Version;

                        version = model.AppVersion;
                    }

                    if (Directory.Exists($"./wwwroot/temp/{Path.GetFileNameWithoutExtension(file.FileName)}"))
                    {
                        DirectoryInfo dir = new DirectoryInfo($"./wwwroot/temp/{Path.GetFileNameWithoutExtension(file.FileName)}");
                        FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();  //返回目录中所有文件和子目录
                        foreach (FileSystemInfo i in fileinfo)
                        {
                            if (i is DirectoryInfo)            //判断是否文件夹
                            {
                                DirectoryInfo subdir = new DirectoryInfo(i.FullName);
                                subdir.Delete(true);          //删除子目录和文件
                            }
                            else
                            {
                                File.Delete(i.FullName);      //删除指定文件
                            }
                        }
                        dir.Delete();
                    }
                }

                AppInfo AppInfo = new AppInfo
                {
                    SeqNo = Guid.NewGuid().ToString("N"),
                    CreateUser = user.UserSubjectId,
                    CreateUserName = user.Name,
                    CreateTime = DateTime.Now,
                    AppName = Path.GetFileName(file.FileName),
                    AppVersion = version,
                    AppMD5 = md5value,
                    AppSize = file.Length,
                    AppType = Path.GetExtension(file.FileName),
                    AppFullName = $"/upload/{model.AppBusinessType}/{DateTime.Now.Year.ToString()}/{DateTime.Now.Month.ToString()}/{model.AppBusinessNo}/{Path.GetFileName(file.FileName)}",
                    AppBusinessType=model.AppBusinessType,
                    AppBusinessNo=model.AppBusinessNo
                };

                var rs = Db.Insertable(AppInfo)
                    .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.APPINFO}")
                    .IgnoreColumns(ignoreNullColumn: true)
                    .ExecuteCommand();

                return new CoreVO<string>()
                {
                    Code = rs > 0 ? "0X00" : "0X01",
                    Success = rs > 0,
                    Msg = rs > 0 ? "发布成功" : "发布失败",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new CoreVO<string>()
                {
                    Code = "0X01",
                    Success = false,
                    Msg = e.Message,
                    Data = null
                };
            }

        }
        #endregion

        #region Update
        /// <summary>
        /// MUpdate
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //public CoreVO<string> MUpdate(AppInfoUpdateDO model,CoreLoginUser user)
        //{
        //    try
        //    {
        //        var data = GetBySeqNo(model.SeqNo);
        //        if (data.Success && data.Data != null)
        //        {
        //            AppInfo AppInfo = new AppInfo
        //            {
        //                UpdateUser = user.UserSubjectId,
        //                UpdateUserName = user.Name,
        //                UpdateTime = DateTime.Now,
        //                SeqNo = model.SeqNo,
        //                Name = model.Name,
        //                Url = model.Url,
        //                Icon = model.Icon,
        //                Summary = model.Summary
        //            };


        //            var rs = Db.Updateable<AppInfo>(AppInfo)
        //                .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.AppInfo}")
        //                .IgnoreColumns(ignoreAllNullColumns: true)
        //                .Where(x => x.SeqNo == AppInfo.SeqNo)
        //                .ExecuteCommand();

        //            return new CoreVO<string>()
        //            {
        //                Code = rs > 0 ? "0X00" : "0X01",
        //                Success = rs > 0,
        //                Msg = rs > 0? "数据更新成功" : "更新失败",
        //                Data = null
        //            };
        //        }
        //        else
        //        {
        //            return new CoreVO<string>()
        //            {
        //                Code = "0X01",
        //                Success = false,
        //                Msg = "更新失败，没有找到数据",
        //                Data = null
        //            };
        //        }


        //    }
        //    catch (Exception e)
        //    {
        //        return new CoreVO<string>()
        //        {
        //            Code = "0X01",
        //            Success = false,
        //            Msg = e.Message,
        //            Data = null
        //        };
        //    }

        //}
        #endregion

        #region Delete
        /// <summary>
        /// MDelete
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //public CoreVO<string> MDelete(string SeqNo)
        //{
        //    try
        //    {
        //        var data = GetBySeqNo(SeqNo);
        //        if (data.Success && data.Data != null)
        //        {

        //            var rs = Db.Deleteable<AppInfo>()
        //            .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.AppInfo}")
        //            .Where(x => x.SeqNo == SeqNo)
        //            .ExecuteCommand();

        //            return new CoreVO<string>()
        //            {
        //                Code = rs > 0 ? "0X00" : "0X01",
        //                Success = rs > 0,
        //                Msg = rs > 0? "数据删除成功": "删除失败",
        //                Data = null
        //            };
        //        }
        //        else
        //        {
        //            return new CoreVO<string>()
        //            {
        //                Code = "0X01",
        //                Success = false,
        //                Msg = "删除失败，没有找到数据",
        //                Data = null
        //            };
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return new CoreVO<string>()
        //        {
        //            Code = "0X01",
        //            Success = false,
        //            Msg = e.Message,
        //            Data = null
        //        };
        //    }

        //}
        #endregion


    }
}
