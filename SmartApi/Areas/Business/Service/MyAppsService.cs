﻿using Microsoft.Extensions.Configuration;
using SmartApi.Areas.Business.Data;
using SmartApi.Areas.Business.Models;
using SmartApi.Models;
using SmartApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Areas.Business.Services
{
    public class MyAppsService: CoreBaseRepository<MyApps>
    {
        #region initialize
        private readonly IConfiguration _configuration;
        public MyAppsService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        #endregion

        #region GetAll
        /// <summary>
        /// GetAll By Page
        /// </summary>
        /// <param name="searchDto"></param>
        /// <returns></returns>
        public CoreVO<List<MyApps>> GetAll(MyAppsSearchDto searchDto)
        {
            try
            {
                int total = 0;
                var list = Db.Queryable<MyApps>()
                    .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.MYAPPS}")
                    .WhereIF(!string.IsNullOrWhiteSpace(searchDto.Name), x => x.Name.Contains(searchDto.Name)).ToList().OrderByDescending(x => x.Weight).ThenBy(x => x.Tag).ToList();
                    //.ToPageList(searchDto.PageIndex, searchDto.PageSize, ref total);

                return new CoreVO<List<MyApps>>() {
                    Code = list.Any() ? "0X00" : "0X01",
                    Msg = list.Any() ? $"{total}" : "没有找到数据",
                    Success = true,
                    Data = list.Any() ? list : null
                };
            }
            catch (Exception e)
            {
                return new CoreVO<List<MyApps>>()
                {
                    Code = "0X01",
                    Success = false,
                    Msg = e.Message,
                    Data = null
                };
            }

        }
        #endregion

        #region GetBySeqNo
        /// <summary>
        /// GetBySeqNo
        /// </summary>
        /// <param name="SeqNo"></param>
        /// <returns></returns>
        public CoreVO<MyApps> GetBySeqNo(string SeqNo)
        {
            try
            {
                var rs = Db.Queryable<MyApps>()
                .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.MYAPPS}")
                .Where(x => x.SeqNo.Equals(SeqNo)).First();

                return new CoreVO<MyApps>()
                {
                    Code = rs != null ? "0X00" : "0X01",
                    Success = rs != null,
                    Msg = rs != null?null:"没有找到数据",
                    Data = rs!=null ? rs : null
                };
            }
            catch (Exception e)
            {
                return new CoreVO<MyApps>()
                {
                    Code = "0X01",
                    Success = false,
                    Msg = e.Message,
                    Data = null
                };
            }

        }
        #endregion

        #region Insert
        /// <summary>
        /// MInsert
        /// </summary>
        /// <param name="SeqNo"></param>
        /// <returns></returns>
        public CoreVO<string> MInsert(MyAppsCreateDO model,CoreLoginUser user)
        {
            try
            {
                MyApps myApps = new MyApps
                {
                    SeqNo = Guid.NewGuid().ToString("N"),
                    CreateUser = user.UserSubjectId,
                    CreateUserName = user.Name,
                    CreateTime = DateTime.Now,
                    Name = model.Name,
                    Url = model.Url,
                    Icon = model.Icon,
                    Summary = model.Summary,
                    Tag = model.Tag,
                    Weight = model.Weight
                };

                var rs=Db.Insertable(myApps)
                    .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.MYAPPS}")
                    .IgnoreColumns(ignoreNullColumn: true)
                    .ExecuteCommand();

                return new CoreVO<string>()
                {
                    Code = rs > 0 ? "0X00" : "0X01",
                    Success = rs > 0,
                    Msg = rs > 0? "数据入库成功": "数据入库成功",
                    Data = null
                };
            }
            catch (Exception e)
            {
                return new CoreVO<string>()
                {
                    Code = "0X01",
                    Success = false,
                    Msg = e.Message,
                    Data = null
                };
            }

        }
        #endregion

        #region Update
        /// <summary>
        /// MUpdate
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CoreVO<string> MUpdate(MyAppsUpdateDO model,CoreLoginUser user)
        {
            try
            {
                var data = GetBySeqNo(model.SeqNo);
                if (data.Success && data.Data != null)
                {
                    MyApps myApps = new MyApps
                    {
                        UpdateUser = user.UserSubjectId,
                        UpdateUserName = user.Name,
                        UpdateTime = DateTime.Now,
                        SeqNo = model.SeqNo,
                        Name = model.Name,
                        Url = model.Url,
                        Icon = model.Icon,
                        Summary = model.Summary,
                        Tag = model.Tag,
                        Weight = model.Weight
                    };


                    var rs = Db.Updateable<MyApps>(myApps)
                        .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.MYAPPS}")
                        .IgnoreColumns(ignoreAllNullColumns: true)
                        .Where(x => x.SeqNo == myApps.SeqNo)
                        .ExecuteCommand();

                    return new CoreVO<string>()
                    {
                        Code = rs > 0 ? "0X00" : "0X01",
                        Success = rs > 0,
                        Msg = rs > 0? "数据更新成功" : "更新失败",
                        Data = null
                    };
                }
                else
                {
                    return new CoreVO<string>()
                    {
                        Code = "0X01",
                        Success = false,
                        Msg = "更新失败，没有找到数据",
                        Data = null
                    };
                }


            }
            catch (Exception e)
            {
                return new CoreVO<string>()
                {
                    Code = "0X01",
                    Success = false,
                    Msg = e.Message,
                    Data = null
                };
            }

        }
        #endregion

        #region Delete
        /// <summary>
        /// MDelete
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CoreVO<string> MDelete(string SeqNo)
        {
            try
            {
                var data = GetBySeqNo(SeqNo);
                if (data.Success && data.Data != null)
                {

                    var rs = Db.Deleteable<MyApps>()
                    .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.SMARTADMIN)}{CoreDbTables.MYAPPS}")
                    .Where(x => x.SeqNo == SeqNo)
                    .ExecuteCommand();

                    return new CoreVO<string>()
                    {
                        Code = rs > 0 ? "0X00" : "0X01",
                        Success = rs > 0,
                        Msg = rs > 0? "数据删除成功": "删除失败",
                        Data = null
                    };
                }
                else
                {
                    return new CoreVO<string>()
                    {
                        Code = "0X01",
                        Success = false,
                        Msg = "删除失败，没有找到数据",
                        Data = null
                    };
                }
            }
            catch (Exception e)
            {
                return new CoreVO<string>()
                {
                    Code = "0X01",
                    Success = false,
                    Msg = e.Message,
                    Data = null
                };
            }

        }
        #endregion

    }
}
