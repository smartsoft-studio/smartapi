﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartApi.Areas.Business.Data;
using SmartApi.Areas.Business.Models;
using SmartApi.Areas.Business.Services;
using SmartApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartApi.Areas.Business.Controllers
{
    [Route("api/[controller]")]
    [ApiController,Authorize]
    public class MyAppsController : CoreBaseController
    {
        #region initialize
        private readonly MyAppsService _service;
        public MyAppsController(MyAppsService myAppsService)
            : base()
        {
            _service = myAppsService;
        }
        #endregion

        #region GetAll
        // GET: api/<MyAppsController>
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get(int pageIndex,int pageSize,string name)
        {
            return Json(_service.GetAll(new MyAppsSearchDto { PageIndex = pageIndex == 0 ? 1:pageIndex,PageSize=pageSize==0?30:pageSize,Name= name }));
        }
        #endregion

        #region Get
        // GET api/<MyAppsController>/5
        /// <summary>
        /// get by seqNo
        /// </summary>
        /// <param name="SeqNo"></param>
        /// <returns></returns>
        [HttpGet("{SeqNo}")]
        public IActionResult Get(string SeqNo)
        {
            return Json(_service.GetBySeqNo(SeqNo));
        }
        #endregion

        #region insert
        // POST api/<MyAppsController>
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] MyAppsCreateDO model)
        {
            GetCurrentUser();
            return Json(_service.MInsert(model, _user));
        }
        #endregion

        #region update
        // PUT api/<MyAppsController>/5
        /// <summary>
        /// update by seqNo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        [HttpPut()]
        public IActionResult Put([FromBody] MyAppsUpdateDO model)
        {
            GetCurrentUser();
            return Json(_service.MUpdate(model, _user));
        }
        #endregion

        #region delete
        // DELETE api/<MyAppsController>/5
        /// <summary>
        /// delete by seqNo
        /// </summary>
        /// <param name="SeqNo"></param>
        /// <returns></returns>
        [HttpDelete("{SeqNo}")]
        public IActionResult Delete(string SeqNo)
        {
            return Json(_service.MDelete(SeqNo));
        }
        #endregion
    }
}
