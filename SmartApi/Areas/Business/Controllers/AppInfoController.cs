﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartApi.Areas.Business.Data;
using SmartApi.Areas.Business.Models;
using SmartApi.Areas.Business.Services;
using SmartApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartApi.Areas.Business.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class AppInfoController : CoreBaseController
    {
        #region initialize
        private readonly AppInfoService _service;
        public AppInfoController(AppInfoService AppInfoService)
            : base()
        {
            _service = AppInfoService;
        }
        #endregion

        #region GetAll
        //// GET: api/<AppInfoController>
        ///// <summary>
        ///// get all
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //public IActionResult Get(int pageIndex,int pageSize,string name)
        //{
        //    return Json(_service.GetAll(new AppInfoSearchDto { PageIndex = pageIndex == 0 ? 1:pageIndex,PageSize=pageSize==0?30:pageSize,Name= name }));
        //}
        #endregion

        #region CheckUpdate
        // GET api/<AppInfoController>/5
        /// <summary>
        /// CheckUpdate
        /// </summary>
        /// <param name="MD5"></param>
        /// <returns></returns>
        [HttpPost,AllowAnonymous,Route("CheckUpdate")]
        public IActionResult CheckUpdate([FromBody] MD5DO mD5DO)
        {
            return Json(_service.CheckUpdate(mD5DO.Md5));
        }
        #endregion

        #region Upload
        // POST api/<AppInfoController>
        /// <summary>
        /// Upload
        /// </summary>
        /// <param name="file"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost,DisableRequestSizeLimit]
        public IActionResult Upload(IFormFile file,AppInfoCreateDO model)
        {
            GetCurrentUser();
            return Json(_service.Upload(file,model, _user));
        }
        #endregion

        #region update
        // PUT api/<AppInfoController>/5
        /// <summary>
        /// update by seqNo
        /// </summary>
        /// <param name="path"></param>
        /// <param name="id"></param>
        /// <param name="value"></param>
        //[HttpPut()]
        //public IActionResult Put([FromBody] AppInfoUpdateDO model)
        //{
        //    GetCurrentUser();
        //    return Json(_service.MUpdate(model, _user));
        //}
        #endregion

        #region delete
        // DELETE api/<AppInfoController>/5
        /// <summary>
        /// delete by seqNo
        /// </summary>
        /// <param name="SeqNo"></param>
        /// <returns></returns>
        //[HttpDelete("{SeqNo}")]
        //public IActionResult Delete(string SeqNo)
        //{
        //    return Json(_service.MDelete(SeqNo));
        //}
        #endregion

        #region download
        [HttpGet,Route("download"),AllowAnonymous]
        // POST api/<AppInfoController>
        /// <summary>
        /// Upload
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IActionResult downloadfile(string path)
        {
            var folder = Path.GetDirectoryName(CoreSettings._uploadPath);
            var file = Path.Combine(folder, $"windows10\\2021\\7\\001\\ubuntu-20.04.2-live-server-amd64_20210715051309185.iso");
            const string mime = "application/octet-stream";

            return PhysicalFile(file, mime);
        }
        #endregion
    }
}
