﻿using SmartApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Areas.Business.Models
{
    public class MyApps : CoreBaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        [MaxLength(255), Required(ErrorMessage = "名称未填写")]
        public string Name { get; set; }

        /// <summary>
        /// Url
        /// </summary>
        [MaxLength(512), Required(ErrorMessage = "Url未填写")]
        public string Url { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        [MaxLength(512), Required(ErrorMessage = "图标未填写")]
        public string Icon { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [MaxLength(512)]
        public string Summary { get; set; }

        /// <summary>
        /// 标签
        /// </summary>
        [MaxLength(255)]
        public string Tag { get; set; }

        /// <summary>
        /// 权重
        /// </summary>
        public int Weight { get; set; }

    }
}
