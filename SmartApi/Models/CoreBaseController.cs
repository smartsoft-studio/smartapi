﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public class CoreBaseController: Controller
    {
        #region initialize

        public CoreLoginUser _user;

        #endregion

        #region GetCurrentUser
        /// <summary>
        /// GetCurrentUser
        /// </summary>
        /// <param name="accesstoken"></param>
        [Route("GetCurrentUser")]
        [HttpGet]
        public void GetCurrentUser()
        {
            var userinfo = User.Claims.FirstOrDefault(x => x.Type == "userinfo").Value;
            var user = JsonConvert.DeserializeObject<CoreUserInfo>(userinfo);
            _user = new CoreLoginUser
            {
                UserSubjectId = user.SubjectId,
                Name = user.Username
            };
        }
        #endregion
    }
}
