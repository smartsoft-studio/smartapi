﻿using System.Collections.Generic;

namespace SmartApi.Models
{
    public class CoreRole
    {
        public string id { get; set; }

        public List<string> operation { get; set; }
    }
}