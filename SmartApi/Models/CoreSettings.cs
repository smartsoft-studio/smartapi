﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public static class CoreSettings
    {
        /// <summary>
        /// 上传文件根目录
        /// </summary>
        public static string _uploadPath { get; set; }

        /// <summary>
        /// initialize
        /// </summary>
        /// <param name="uploadPath"></param>
        public static void Initialize(string uploadPath)
        {
            DirectoryInfo directoryInfo= new DirectoryInfo(uploadPath);
            if (!directoryInfo.Exists)
                Directory.CreateDirectory(directoryInfo.FullName);
            _uploadPath = uploadPath;
        }
    }
}
