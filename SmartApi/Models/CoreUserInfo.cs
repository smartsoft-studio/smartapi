﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public class CoreUserInfo
    {
        public string SubjectId { get; set; }
        public string Username { get; set; }

        public string ProviderName { get; set; }

        public bool IsActive { get; set; }
    }
}
