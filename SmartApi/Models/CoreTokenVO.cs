﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public class CoreTokenVO
    {
        /// <summary>
        /// access token
        /// </summary>
        public string AccessToken
        {
            get;
            set;
        }

        /// <summary>
        /// RefershToken
        /// </summary>
        public string RefreshToken
        {
            get;
            set;
        }

        /// <summary>
        /// token 过期时间
        /// </summary>
        public DateTime ExpireAt
        {
            get;
            set;
        }
        
        /// <summary>
        /// 用户信息
        /// </summary>
        public CoreUserVO user { get; set; }

        /// <summary>
        /// 用户权限
        /// </summary>
        public List<CorePermission> permissions { get; set; }

        /// <summary>
        /// 用户角色
        /// </summary>
        public List<CoreRole> roles { get; set; }
    }
}
