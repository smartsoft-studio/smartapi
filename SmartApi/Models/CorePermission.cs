﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public class CorePermission
    {
        public string id { get; set; }

        public List<string> operation { get; set; }
    }
}
