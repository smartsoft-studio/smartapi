﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    //
    // Summary:
    //     数据库类型
    public enum CoreDBType
    {

        //
        // Summary:
        //     ORACLE
        ORACLE = 0,
        //
        // Summary:
        //     SQLSERVER
        SQLSERVER = 1,
        //
        // Summary:
        //     MYSQL
        MYSQL = 2
    }
}
