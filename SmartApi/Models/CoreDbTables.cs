﻿using Microsoft.Extensions.Configuration;
using SmartApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public static class CoreDbTables
    {

        #region Db Prefix
        /// <summary>
        /// 认证库
        /// </summary>
        public const string IDENTITYSERVER = "IdentityServer";

        /// <summary>
        /// smartadmin 业务库
        /// </summary>
        public const string SMARTADMIN = "smartadmin";

        /// <summary>
        /// MSSQL Db Prefix
        /// </summary>
        private const string DbMSSQL = ".dbo.";

        /// <summary>
        /// MySQL Db Prefix
        /// </summary>
        private const string DbMySQL = ".";

        #endregion

        #region Tables
        /// <summary>
        /// 用户信息
        /// </summary>
        public const string SYSUSERINFO = "SysUserInfo";

        /// <summary>
        /// 我的应用
        /// </summary>
        public const string MYAPPS = "MyApps";

        /// <summary>
        /// smarttools 应用
        /// </summary>
        public const string APPINFO = "AppInfo";

        #endregion

        #region Get Db Prefix
        /// <summary>
        /// Get Db Prefix
        /// </summary>
        /// <param name="dbType"></param>
        /// <returns></returns>
        public static string Db(CoreDBType dbType,string Db)
        {
            switch (dbType)
            {
                case CoreDBType.SQLSERVER:
                    return $"{Db}{DbMSSQL}";
                case CoreDBType.MYSQL:
                    return $"{Db}{DbMySQL}";
                default:
                    return "";
            }
        }
        #endregion

    }
}
