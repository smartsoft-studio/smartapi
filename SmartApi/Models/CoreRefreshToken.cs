﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public class CoreRefreshToken
    {
        public string refreshtoken { get; set; }
    }
}
