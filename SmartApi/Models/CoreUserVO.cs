﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public class CoreUserVO
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string avatar { get; set; }

        /// <summary>
        /// 住址
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// 位置
        /// </summary>
        public string position { get; set; }
    }
}
