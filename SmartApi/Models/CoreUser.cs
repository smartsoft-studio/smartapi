﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public class CoreUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
