﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Models
{
    public class CorePermissionsVO
    {
        public List<CorePermission> permissions { get; set; }
    }
}
