using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using SmartApi.Extensions;
using SmartApi.Models;
using SqlSugar.IOC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SmartApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore()
               .AddAuthorization();


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(
                    JwtBearerDefaults.AuthenticationScheme, options =>
                    {
                        options.Authority = Configuration.GetSection("Authority").Value;
                        options.RequireHttpsMetadata = bool.Parse(Configuration.GetSection("RequireHttpsMetadata").Value);
                        options.Audience = Configuration.GetSection("Audience").Value;
                        //options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                        //{
                        //    ValidateAudience = false
                        //};
                        options.TokenValidationParameters.ClockSkew = TimeSpan.FromHours(double.Parse(Configuration.GetSection("ClockSkew").Value));
                        options.TokenValidationParameters.RequireExpirationTime = true;
                    }
                );
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SmartApi", Version = "v1" });
                c.DocumentFilter<CoreHiddenApiFilter>();
                var securityScheme = new OpenApiSecurityScheme
                {
                    Name = "JWT Authentication",
                    Description = "Enter JWT Bearer token **_only_**",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    BearerFormat = "JWT",
                    Reference = new OpenApiReference
                    {
                        Id = JwtBearerDefaults.AuthenticationScheme,
                        Type = ReferenceType.SecurityScheme
                    }
                };
                c.AddSecurityDefinition(securityScheme.Reference.Id, securityScheme);
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    { securityScheme, new string[] { } }
                });
                var xmlFilePath = Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
                c.IncludeXmlComments(xmlFilePath);
            });

            services.AddCors(options => {
                options.AddPolicy("SmartApi",
                builder => builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());
            });

            services.AddSqlSugar(new IocConfig()
            {
                ConnectionString = Configuration.GetSection("ConnectionConfig").GetSection("ConnectingString").Value,
                DbType = IocDbType.MySql,
                IsAutoCloseConnection = true//自动释放
            });

            services.AddIoc(this, "SmartApi", x => x.Name.Contains("Service"));

            //services.AddHttpClient("xxHttpClient", x =>
            //{
            //    var uri = Configuration.GetSection("swHost").Value;
            //    if (!string.IsNullOrWhiteSpace(uri))
            //    {
            //        x.BaseAddress = new Uri(uri);
            //    }
            //});
            services.AddHttpClient("IdpHttpClient", x =>{});


            //取消大文件限制
            services.Configure<FormOptions>(x => {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue;

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            CoreSettings.Initialize($"{env.WebRootPath}{Path.DirectorySeparatorChar}upload{Path.DirectorySeparatorChar}");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "/swagger/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c => {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "SmartApi v1");
                    c.RoutePrefix = string.Empty;
             }) ;

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors("SmartApi");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
