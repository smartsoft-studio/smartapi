﻿using IdentityModel.Client;
using SmartApi.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using SmartApi.Services;

namespace SmartApi.Controllers
{
    [Route("api/identity")]
    [Authorize]
    public class CoreIdentityController : Controller
    {

        #region initialize
        private readonly CoreIdentityService _coreIdentityService;
        /// <summary>
        /// initialize
        /// </summary>
        /// <param name="configuration"></param>
        public CoreIdentityController(CoreIdentityService coreIdentityService)
        {
            _coreIdentityService = coreIdentityService;
        }
        #endregion

        #region GetClaims
        /// <summary>
        /// GetClaims
        /// </summary>
        /// <returns></returns>
        //[HttpGet]
        //public IActionResult Get()
        //{
        //    //var data =User.Claims.Where(x=>x.Type.Equals("userinfo")).Select(x=>new {x.Value}).FirstOrDefault().ToString().Split("=")[1].Substring(1);

        //    var data =User.Claims.Where(x=>x.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")).Select(x=>new {x.Value}).FirstOrDefault().ToString().Split("=")[1].Substring(1);

        //    //var rs = JsonConvert.DeserializeObject<CoreUserInfo>(data.Substring(0, data.Length - 2));

        //    var rs = data.Substring(0, data.Length - 2);

        //    //var rs = User.Claims.Select(x => new { x.Type, x.Value });

        //    return Json(rs);
        //}
        #endregion

        #region Login
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost, Route("Login")]
        [AllowAnonymous]
        public IActionResult Login([FromBody]CoreUser user)
        {
            var result = _coreIdentityService.GetTokenByPasswordAsync(user).Result;
            return Json(result);
        }
        #endregion

        #region LoginByRefreshToken
        /// <summary>
        /// LoginByRefreshToken
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("LoginByRefreshToken")]
        [AllowAnonymous]
        public IActionResult LoginByRefreshToken([FromBody]CoreRefreshToken refreshToken)
        {
            var result = _coreIdentityService.GetTokenByRefreshTokenAsync(refreshToken.refreshtoken).Result;

            return Json(result);
        }
        #endregion

        #region Logout
        //[Route("LogOut"),HttpPost,AllowAnonymous]
        //public IActionResult LogOut()
        //{
        //    return null;
        //}
        #endregion
    }
}
