﻿using SqlSugar;
using SqlSugar.IOC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Repository
{
    public class CoreBaseRepository<T> : SimpleClient<T> where T : class, new()
    {
        public ISqlSugarClient Db;
        public CoreBaseRepository(ISqlSugarClient context = null) : base(context)//默认值等于null
        {
            base.Context = DbScoped.Sugar;
            Db = DbScoped.Sugar;
        }

    }
}
