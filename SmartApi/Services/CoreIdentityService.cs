﻿using IdentityModel.Client;
using SmartApi.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using SmartApi.Repository;
using System.IdentityModel.Tokens.Jwt;

namespace SmartApi.Services
{
    public class CoreIdentityService : CoreBaseRepository<CoreLoginUser>
    {
        #region initialize
        private readonly IHttpClientFactory _httpClientFactory;

        private readonly HttpClient _idpClient;
        private readonly IConfiguration _configuration;

        public CoreIdentityService(IHttpClientFactory httpClientFactory,IConfiguration configuration)
        {
            _httpClientFactory = httpClientFactory;
            _idpClient = _httpClientFactory.CreateClient("IdpHttpClient");
            _configuration = configuration;
        }
        #endregion

        #region GetToken
        public async Task<CoreVO<CoreTokenVO>> GetTokenByPasswordAsync(CoreUser user)
        {
            CoreVO<CoreTokenVO> CoreVO = new CoreVO<CoreTokenVO>();
            try
            {
                TokenResponse responseToken = await _idpClient.RequestPasswordTokenAsync(new PasswordTokenRequest
                {
                    Address = $"{_configuration.GetSection("Authority").Value}/connect/token",
                    ClientId = _configuration.GetSection("ClientId").Value,
                    ClientSecret = _configuration.GetSection("ClientSecret").Value,
                    Scope = string.Join(' ', _configuration.GetSection("Scopes").Value.Split(',').ToList()),
                    UserName = user.UserName,
                    Password = user.Password
                });

                if (responseToken.IsError)
                {
                    CoreVO.Code = "-1";
                    CoreVO.Msg = responseToken.Error + responseToken.ErrorDescription;
                }
                else
                {
                    CoreLoginUser coreLogin = GetLoginUser(responseToken.AccessToken);

                    CoreVO.Code = "0";
                    CoreVO.Success = true;
                    CoreVO.Msg = GetWelcomeMsg();
                    CoreVO.Data = new CoreTokenVO
                    {
                        AccessToken = responseToken.AccessToken,
                        ExpireAt = DateTime.Now.AddSeconds(responseToken.ExpiresIn),
                        RefreshToken = responseToken.RefreshToken,
                        user=new CoreUserVO
                        {
                            name=coreLogin.Name,
                            avatar = coreLogin.Avatar,
                            address = coreLogin.Address,
                            position = coreLogin.Position
                        },
                        permissions = new List<CorePermission> {
                            new CorePermission
                            {
                                id = "queryForm",
                                operation = new List<string>
                                {
                                    "add",
                                    "edit"
                                }
                            }
                        },
                        roles = new List<CoreRole>
                        {
                            new CoreRole
                            {
                                id = "admin",
                                operation = new List<string>
                                {
                                    "add",
                                    "edit",
                                    "delete"
                                }
                            }
                        }
                    };
                }

                return await Task.FromResult(CoreVO);
            }
            catch (Exception ex)
            {
                CoreVO.Code = "-1";
                CoreVO.Msg = ex.Message;
                return await Task.FromResult(CoreVO);
            }
        }
        #endregion

        #region GetByRefreshToken
        public async Task<CoreVO<CoreTokenVO>> GetTokenByRefreshTokenAsync(string refreshToken)
        {
            CoreVO<CoreTokenVO> CoreVO = new CoreVO<CoreTokenVO>();
            try
            {
                TokenResponse responseToken = await _idpClient.RequestRefreshTokenAsync(new RefreshTokenRequest
                {
                    Address = $"{_configuration.GetSection("Authority").Value}/connect/token",
                    ClientId = _configuration.GetSection("ClientId").Value,
                    ClientSecret = _configuration.GetSection("ClientSecret").Value,
                    Scope = string.Join(' ', _configuration.GetSection("Scopes").Value.Split(',').ToList()),
                    RefreshToken = refreshToken
                });
                if (responseToken.IsError)
                {
                    CoreVO.Code = "-1";
                    CoreVO.Msg = responseToken.Error + responseToken.ErrorDescription;
                }
                else
                {
                    CoreLoginUser coreLogin = GetLoginUser(responseToken.AccessToken);

                    CoreVO.Code = "0";
                    CoreVO.Success = true;
                    CoreVO.Msg = GetWelcomeMsg();
                    CoreVO.Data = new CoreTokenVO
                    {
                        AccessToken = responseToken.AccessToken,
                        ExpireAt = DateTime.Now.AddSeconds(responseToken.ExpiresIn),
                        RefreshToken = responseToken.RefreshToken,
                        user = new CoreUserVO
                        {
                            name = coreLogin.Name,
                            avatar = coreLogin.Avatar,
                            address = coreLogin.Address,
                            position = coreLogin.Position
                        },
                        permissions = new List<CorePermission> {
                            new CorePermission
                            {
                                id = "queryForm",
                                operation = new List<string>
                                {
                                    "add",
                                    "edit"
                                }
                            }
                        },
                        roles = new List<CoreRole>
                        {
                            new CoreRole
                            {
                                id = "admin",
                                operation = new List<string>
                                {
                                    "add",
                                    "edit",
                                    "delete"
                                }
                            }
                        }
                    };
                }

                return await Task.FromResult(CoreVO);
            }
            catch (Exception ex)
            {
                CoreVO.Code = "-1";
                CoreVO.Msg = ex.Message;
                return await Task.FromResult(CoreVO);
            }
        }
        #endregion

        #region GetLoginUser
        /// <summary>
        /// GetLoginUser
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public CoreLoginUser GetLoginUser(string accessToken)
        {
            var token = new JwtSecurityToken(accessToken);
            var sub = token.Claims.FirstOrDefault(x => x.Type == "sub").Value;

            return Db.Queryable<CoreLoginUser>()
                .AS($"{CoreDbTables.Db((CoreDBType)Enum.Parse(typeof(CoreDBType), _configuration["ConnectionConfig:DBType"]), CoreDbTables.IDENTITYSERVER)}{CoreDbTables.SYSUSERINFO}")
                .Where(x => x.UserSubjectId.Equals(sub))
                .First();
        }
        #endregion

        #region GetWelcomeMsg
        /// <summary>
        /// GetWelcomeMsg
        /// </summary>
        /// <returns></returns>
        public string GetWelcomeMsg()
        {
            List<string> timeList = new List<string> {
                $"早上好,欢迎回来",
                $"上午好,欢迎回来",
                $"中午好,欢迎回来",
                $"下午好,欢迎回来",
                $"晚上好,欢迎回来",
                $"夜深了,早点休息呦"
            };

            var hour=int.Parse(DateTime.Now.AddHours(8).Hour.ToString());

            return
                 hour <  5  ? timeList[5] :
                (hour <= 9  ? timeList[0] :
                (hour <= 11 ? timeList[1] :
                (hour <= 13 ? timeList[2] :
                (hour <= 18 ? timeList[3] :
                (hour <= 21 ? timeList[4] :
                              timeList[5])))));
        }
        #endregion
    }
}
