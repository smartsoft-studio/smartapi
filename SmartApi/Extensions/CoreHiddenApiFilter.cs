﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartApi.Extensions
{
    public class CoreHiddenApiFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            foreach (var apiDescription in context.ApiDescriptions)
            {
                //忽略列表
                List<string > ignoreList = new List<string> {
                    "GetCurrentUser"
                };

                foreach (var item in ignoreList)
                {
                    if (apiDescription.RelativePath.Contains(item))
                    {
                        string key = $"/{apiDescription.RelativePath}";
                        swaggerDoc.Paths.Remove(key);
                    }
                }

            }
        }
    }
}
